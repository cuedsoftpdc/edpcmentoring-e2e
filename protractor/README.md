# EDPCMentoring end to end test

## Description
This repository is seperate from the main EDPCMentoring application repository. 

It contains the e2e tests to be run against a running EDPCMentoring test or development instance.

Later this repository could be merged into the main EDPCMentoring repository so that the e2e tests and the application reside in the same location, however for the time being they are seperate.
    
## Key stakeholders
    
Development team for EDPCMentoring (they will have the definition of done for the stories/tasks/bugs the tests are targeting)

## Documentation

A list of the tests covered can be found in [plan.md](plan.md)

Guide to running these tests can be found further down this [README.md](README.md)


## Languages

Javascript

## Installation notes 

The scripts require protractor to be installed


    $ npm install -g protractor


requried for the Jasime custom reporter:


    $ npm install -g fs
    $ npm install protractor-jasmine2-html-reporter
    $ npm install decache


A selenium server / grid is also required


    $ webdriver-manager update

    

### Configuration files/variables
Configuration variables required:

* PROT_SELADDRESS eg http://localhost:4444/wd/hub
* PROT_BROWSER eg firefox
* PROT_BASEURL eg http://my-test-instance-url
* PROT_DIRECT false/true whether connect to the browser directly
    
    The main config file is config.js 

### Test
These are test scripts

#### Dependencies
* nodejs 
* protractor
* a running instance of EDPCMentoring (PROT_BASEURL) 

#### Commands

To setup the environment see [Installation notes][Installation notes]

To check the setup:

Start the webdriver server:

    $ webdriver-manager start

Run the tests:


    PROT_SELADDRESS="your/wd/hub" PROT_BROWSER=firefox PRO_BASEURL="your/site/url" PROT_DIRECT=false protractor config.js --specs specs/test_file.js


eg

    PROT_SELADDRESS="http://localhost:4444/wd/hub" PROT_BROWSER=firefox 
    PROT_BASEURL="http://<your test server : port>" PROT_DIRECT=false protractor conf.js --specs specs/test_access.js



To run all tests:


    PROT_SELADDRESS="your/wd/hub" PROT_BROWSER=firefox \
    PROT_BASEURL="your/site/url" PROT_DIRECT=false protractor conf.js 


To run an individual test:


    PROT_SELADDRESS="your/wd/hub" PROT_BROWSER=firefox \
    PROT_BASEURL="your/site/url" PROT_DIRECT=false protractor conf.js --specs specs/<name of file>.js


## Contributing to the code base

* Clone this repo
* Create a feature branch
* Submit a pull request

## Coding styles

General guidance: [Carman's style guide](https://www.protractortest.org/#/style-guide)


## Tests
These are tests!

## Issue tracking
The test plan Plan.md was populated from issues on the Jira board

## Maintenance
The tests will be called as part of a regular CI process, required prior to brnach merge and release.

## History

## License
(is there one?)

To improve the layout of this markdown file, add HTML, code snippets and links etc please try: [a markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
