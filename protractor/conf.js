

var fs = require('fs');
// NB also used in RECK 
// -> could we create a module to include in all our protractor projects?

var reporter = {
  specDone: function (result) {
      if (result.failedExpectations.length > 0) {
          browser.getProcessedConfig().then(function (config) {
              browser.takeScreenshot().then(function (png) {
                  var dirPath = './reports/screenshots/';
                  if (!fs.existsSync('./reports')) {
                      fs.mkdirSync('./reports');
                      if (!fs.existsSync(dirPath))
                          fs.mkdirSync(dirPath);
                  }
                  var fileName = (config.capabilities.browserName + '-' + result.fullName).replace(/[\/\\]/g, ' ').substring(0, 230);
                  var stream = fs.createWriteStream(dirPath + fileName + '.png');
                  stream.write(new Buffer(png, 'base64'));
                  stream.end();
              }, function (error) {
                  console.log("failed to take screenshot");
              })
          })
      }
  }
} 

var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

// An example configuration file.
exports.config = {
  directConnect: (  process.env.PROT_SELADDRESS ) ? false : true,
  seleniumAddress:  process.env.PROT_SELADDRESS || 'http://selenium-vm.eng.cam.ac.uk:4444/wd/hub',

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': process.env.PROT_BROWSER || 'chrome',
    //'browserName': 'firefox'

    chromeOptions: {
      args: [ "--window-size=1920,1080" ]
    },

    'moz:firefoxOptions': {
      args: [ "--window-size=1920,1080" ]
    }
  },

  // Framework to use. Jasmine is recommended.
  framework: 'jasmine',

  // Spec patterns are relative to the current working directory when
  // protractor is called.
  specs: ['specs/*js'],

  // Options to be passed to Jasmine.
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  },

  //params section - used to setup globals in the tests
  params: {
    baseurl: process.env.PROT_BASEURL || "http://sms67pc.eng.cam.ac.uk:3001"
  },

  onPrepare: function(){
//    jasmine.getEnv().addReporter(reporter); used a self written reporter
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: './J2H_reports',
        takeScreenshots: true,
        takeScreenshotsOnlyOnFailures: true
      })
    );
  }
};

if (process.env.PROT_DIRECT == false){
//    remove key from export.config

}