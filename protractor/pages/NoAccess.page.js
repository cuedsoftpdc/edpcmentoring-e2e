'use strict'

var noAccessPage = function() {
    
  var AccessText = element(by.cssContainingText("h1","You do not have access to this page"))


  this.getAccessText = async function(){
    return AccessText.getText()
  }

  this.clickLogout = async function(){
    await logout.click()
  }

};
module.exports = new noAccessPage();