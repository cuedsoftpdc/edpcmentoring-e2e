'use strict'

var AdminPage = function() {
    
  var AdminTitle = element(by.css("a[href='/admin/']"))


  this.getAdminTitleText = async function(){
    return AdminTitle.getText()
  }
};
module.exports = new AdminPage();