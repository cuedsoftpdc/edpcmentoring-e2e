'use strict'

var HomePage = function() {
  
  var pageTitle = element(by.css('.campl-page-title'));
  var adminTab = element(by.css("a[href='/admin/']"));
  var homeTab = element(by.css("a[href='/']"));
  var logoutBtn = element(by.css(("a[href='/accounts/logout/']")));
  var matchMaker = element(by.css(("a[href='/matching/']")));
  var toggleMentorReqs = element(by.id("toggleMentorReqs"))
  var toggleMenteeReqs = element(by.id("toggleMenteeReqs"))
  var MentorReqsType = element(by.css("[ng-model=\"me.mentorship_preferences.mentor_requirements\"]"))
  var MenteeReqsType = element(by.css("[ng-model=\"me.mentorship_preferences.mentee_requirements\"]"))
  var SendInvite = element(by.cssContainingText("a","Send Invite"))
  var InvitationsFromMenteesButton = element(by.css("a[ng-click=\"manageMyMenteeInvites()\"]"))
  var endMentorRelationship = element(by.css('a[ng-click=\"removeMentor(mymentor)\"]'))
  var endMenteeRelationship = element(by.css('a[ng-click="removeMentee(mymentee)"]'))
  var manageMenteeInvites = element(by.css('a[ng-click="manageMyMenteeInvites()"]'))
  var manageMentorInvites = element(by.css('a[ng-click="manageMyMentorInvites()"]'))



  this.getTitleText = function(){
      return pageTitle.getText()
  }

  this.getAdminTabText = function(){
      return adminTab.getText()
  }

  this.getHomeTabText = function(){
      return homeTab.getText()
  }

  this.getUserMenu = function(name){
      return element(by.cssContainingText("a[style='position: relative;']",name))
  }

  this.clickAdmintab = async function(){
    await adminTab.click()
  }

  this.clickMatchMakerTab = async function(){
    await matchMaker.click()
  }


  this.clickLogout = async function(name){
    await this.getUserMenu(name).click()
    await logoutBtn.click()
  }

  this.getNumAdminTabs = async function(){
    return element.all(by.css("a[href='/admin/']"))
  }

  this.getNumMatchMakerTabs = async function(){
    return element.all(by.css("a[href='/matching/']"))
  }

  this.getToggleMentorReqsText= async function(){
    return toggleMentorReqs.getText()
  }

  this.clickToggleMentorReqs = async function(){
    await toggleMentorReqs.click()
  }

  this.clickToggleMenteeReqs = async function(){
    await toggleMenteeReqs.click()
  }

  this.getToggleMenteeReqsText= async function(){
    return toggleMenteeReqs.getText()
  }

  this.clickInviteMentor = async function(){
    await element(by.cssContainingText("a","Invite someone else to be your mentor")).click()
  }

  this.clickInviteMentee = async function(){
    var x = await element(by.cssContainingText('a','Invite someone else to be your mentee'))
    await x.click()
  }

  this.getNumNames = async function(name){
    return element.all(by.cssContainingText("td",name))
  }

  this.setMentorSearchingTrue = async function(){
    var x = await this.getToggleMentorReqsText()
    if (x == String("are not")){
      await this.clickToggleMentorReqs()
    }
  }

  this.setMentorSearchingFalse = async function(){
    var x = await this.getToggleMentorReqsText()
    if (x == String("are")){
      await this.clickToggleMentorReqs()
    }
  }

  this.setMenteeSearchingTrue = async function(){
    var x = await this.getToggleMenteeReqsText()
    if (x == String("are not")){
      await this.clickToggleMenteeReqs()
    }
  }


  this.setMenteeSearchingFalse = async function(){
    var x = await this.getToggleMenteeReqsText()
    if (x == String("are")){
      await this.clickToggleMenteeReqs()
    }
  }

  this.getNumHiddenMentorReqsCommas = async function(){
    var x = await element.all(by.css("[ng-show=\"me[\'mentorship_preferences\'][\'is_seeking_mentor\']\"][class=\"ng-hide\"]"))
    return x
  }

  this.typeInMentorReqs = async function(WhatToType){
    await MentorReqsType.clear()
    await MentorReqsType.sendKeys(WhatToType)
  }

  this.getNumHiddenMenteeReqsCommas = async function(){
    var x = await element.all(by.css("[ng-show=\"me[\'mentorship_preferences\'][\'is_seeking_mentee\']\"][class=\"ng-hide\"]"))
    return x
  }

  this.typeInMenteeReqs = async function(WhatToType){
    await MenteeReqsType.clear()
    await MenteeReqsType.sendKeys(WhatToType)
  }

  this.clickOnMenteeInvite = async function(name){
    await element(by.cssContainingText("tr",name)).click()

  }

  this.clickOnMentorInvite = async function(name){
    await element(by.cssContainingText("tr",name)).click()

  }

  this.clickOnSendInvite = async function(){
    await SendInvite.click()
  }


  this.clickOnInvitationsFromMenteesButton = async function(){
    await InvitationsFromMenteesButton.click()
  }

  this.getNumMentorRelationships = async function(){
    var x = await element.all(by.css("a[ng-click=\"removeMentor(mymentor)\"]"))
    return x.length
  }

  this.getNumMenteeRelationships = async function(){
    var x = await element.all(by.css("a[ng-click=\"removeMentee(mymentee)\"]"))
    return x.length
  }

  this.removeAMentor = async function(){
    await endMentorRelationship.click()
    await element(by.cssContainingText("a","Yes remove now")).click()
  }

  this.removeAMentee = async function(){
    await endMenteeRelationship.click()
    await element(by.cssContainingText("a","Yes remove now")).click()
  }

  this.clickOnManageMenteeInvites = async function(){
    await manageMenteeInvites.click()
  }

  this.getNumHiddenManageMenteeInvites = async function(){
    var x = await element.all(by.css("[class=\"ng-hide\"][ng-show=\" invitations.myMentee.length>0 \"]"))
    return x.length
  }

  this.removeMentorInvite = async function(){
    await element(by.cssContainingText("a","Decline")).click()
  }

  this.getNumHiddenManageMentorInvites = async function(){
    var x = await element.all(by.css("[class=\"ng-hide\"][ng-show=\" invitations.myMentor.length>0 \"]"))
    return x.length
  }

  this.removeMenteeInvite = async function(){
    await element(by.cssContainingText("a","Decline")).click()
  }

  this.clickOnManageMentorInvites = async function(){
    await manageMentorInvites.click()
  }

  this.acceptInvite = async function(){
    await element(by.cssContainingText("a","Accept")).click()
    await element(by.cssContainingText("a","Close")).click()
  }

  this.getNumNamePartners = async function(name){
    var x = await element.all(by.cssContainingText("tbody",name))
    return x.length
  }

  this.removeARelationship = async function(){


  }

};
module.exports = new HomePage();