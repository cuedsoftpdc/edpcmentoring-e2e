'use strict'

var LoginPage = function() {
  
  var userId = element(by.id('userid'));
  var password = element(by.id('pwd'));
  var loginBtn = element(by.name('submit'));

  this.setName = async function(name) {
    await userId.sendKeys(name);
  };
  
  this.setPassword = async function(pwd) {
    await password.sendKeys(pwd);
  };
  
  this.clickOnLoginBtn = async function() {
    await loginBtn.click();
  };
  
  this.logon = async function(username,password) {
    
    browser.waitForAngularEnabled(false);
    await this.setName(username);
    await this.setPassword(password);
    await this.clickOnLoginBtn();
    browser.waitForAngularEnabled(true);
  }
};
module.exports = new LoginPage();
