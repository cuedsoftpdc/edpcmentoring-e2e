'use strict'

var logoutpage = function() {
  
  var logout = element(by.css("a[href='logout.html']"))

  this.clickLogout = async function(){
    await logout.click()
  }

};
module.exports = new logoutpage();