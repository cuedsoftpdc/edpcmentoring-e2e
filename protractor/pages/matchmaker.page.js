'use strict'

var MatchMakerPage = function() {
    
  var MatchMakerTitle = element(by.cssContainingText("h1","Match mentors and mentees"))
  var createMentorMentee = element(by.css("a[ng-click=\"addMentee(selected_seeker.user)\"][ng-show=\"selected_seeker\"]"))
  var createMenteeMentor = element(by.css("a[ng-click=\"addMentor(selected_seeker.user)\"][ng-show=\"selected_seeker\"]"))

  this.getMatchMakerTitleText = async function(){
    return MatchMakerTitle.getText()
  }

  this.clickOnName = async function(name){
    await element(by.cssContainingText("tr",name)).click()
  }

  this.clickOnNameInDialog = async function(name){
    await element(by.cssContainingText(".ngdialog-content tr",name)).click()
  }

  this.clickOnCreateMentorMentee = async function(){
    await createMentorMentee.click()
  }

  this.clickOnCreateMenteeMentor = async function(){
    await createMenteeMentor.click()
  }








};
module.exports = new MatchMakerPage();