# Test plan

loaded during integration tests:

Users  - Personae:

* test0001 admin and active matchmaker
* test0009 admin and active matchmaker
* test0002 active, regular user
* test0010 inactive user
* test0012 active matchmaker user TODO


## Basic Access - test_access.js

1. Inactive users are denied access [x]
2. Active users are provided access [x]
3. Active admin users are provided access to admin pages[x]
4. Active admin users are provided access to matchmaker pages[x]
5. Active matchmaker has access to matchmaker pages[x]
6. Non admin user restricted access to admin pages[x]
7. Non-matchmaker user is restricted access to admin pages[x]
8. Users are able to log out [x]


## Toggling Availability - test_toggle.js


1. Logged in mentee can toggle status
2. Logged in mentor can toggle status
3. Toggling status results in toggling invites/requirements appearing/dissapearing
4. Mentor can see mentee has become available
5. Mentor can see mentee has become unavailable
6. Mentee can see mentor has become available
7. Mentee can see mentor has become unavailable

## Managing Relationships

1. Mentor can invite mentee [x]
2. mentee can accept mentor invite [x]
3. mentor can see mentee requirements when inviting [x]
4. Mentor can remove a relationship [x]
5. Mentee can invite mentor [x]
6. Mentor can accept mentee invite [x]
7. mentee can see mentor requirements when inviting [x]
8. mentee can remove a relationship [x]

## Matchmaker - test_matchmaker.js

1. Matchmaker can start a relationship between a mentor and a mentee [x]
2. Matchmaker can start a relationship between a mentee and a mentor [x]

## Managing Users - managing_users.js

A user with an admin role can:

1. Provide matchmaker access to users 
2. Remove matchmaker access from users
3. Access the admin interface
4. Access to matchmaker pages
5. Add Users
6. Look at the latest version of the database
7. Look at recent actions
8. Look at the history of any objects in the database
9. Full control over all objects in the database

## Matchmaker User

A user with an Matchmaker role can:

1. Access to mathmaker pages
2. Match mentors and mentees
3. See a list of mentees and mentors available
4. Can see a users requirements
5. Can see any pending invitations for a user
7. Can comfirm or decline any pending invitations
7. Can Pair a mentor to a mentee

## Student User

1. Toggle whether they are seeking a mentor/mentee
2. Can add special mentor/mentee requirements
3. can add meeting with mentors/mentees
4. End relationships with mentors/mentees
5. Check past meetings with mentors/mentees
6. Invite someone to be a mentor/mentee
7. List of mentors/mentees showing who would are seeking listing their requirements
8. User should see their mentors and mentees with basic details about each one
9. User should then be able to send invitations to mentors/mentees
10. User should be be able to see invitations sent
11. user can see who has invited them and accept or reject invitations
12. When user sees invites should see the relevant description
13. See number of sent and recieved invitations
14. 
15. 

## Database

1. Show latest up to date information on mentors and mentees
2. 
3. 


## Bug to Resolve

1. Message if mentees send invitations should not read "You have offers from mentors" should read "Offers from mentees"
2. One person should only be capable of inviting one person once
