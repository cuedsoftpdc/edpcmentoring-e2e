//https://github.com/angular/protractor/issues/3881#


'use strict'

var decache = require('decache')

/*

var op = require('./objects.page.js');
op.restartAll()

// replace 
// await loginPage.logon("test0001","test")
// with
// await op.loginPage.logon("test0001","test")
*/

module.exports = function() {

  this.loginPage;
  this.homePage;
  this.logoutPage;
  this.NoAccessPage;
  this.AdminPage;
  this.matchMakerPage;


  this.getLoginPage = function(){
    decache('../pages/login.page.js')
    var LoginPage = require("../pages/login.page.js");
    return LoginPage;
  }

  this.getHomePage = function(){
    decache('../pages/home.page.js')
    var HomePage = require("../pages/home.page.js");
    return HomePage;
  }

  this.getLogoutPage = function(){
    decache('../pages/logout.page.js')
    var LogoutPage = require("../pages/logout.page.js");
    return LogoutPage;
  }

  this.getNoAccessPage = function(){
    decache('../pages/NoAccess.page.js')
    var NoAccessPage = require("../pages/NoAccess.page.js");
    return NoAccessPage;
  }

  this.getAdminPage = function(){
    decache('../pages/admin.page.js')
    var AdminPage = require("../pages/admin.page.js");
    return AdminPage;
  }

  this.getMatchMakerPage = function(){
    decache('../pages/matchmaker.page.js')
    var MatchMakerPage = require("../pages/matchmaker.page.js");
    return MatchMakerPage;
  }

  this.restartAll = async function(){

    await browser.restart()
    this.homePage=this.getHomePage()
    this.loginPage=this.getLoginPage()
    this.homePage=this.getHomePage()
    this.logoutPage=this.getLogoutPage()
    this.noAccessPage=this.getNoAccessPage()
    this.adminPage=this.getAdminPage()
    this.matchMakerPage=this.getMatchMakerPage()

    browser.waitForAngularEnabled(false);
    await browser.get(browser.params.baseurl);
  }

};
//module.exports = new objectspage();