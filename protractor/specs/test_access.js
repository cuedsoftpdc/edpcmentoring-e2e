var objects = new require('./objects.page.js');
var op = new objects();

describe('when an admin (test0001) attempts logs in', function() {


  // We could return a promises from our loginPage.login function
  // it('should open a browser and login', async() => {
  //   then
  //   let homepage = await loginPage.login("test0001","test");
  //var loginPage;

  beforeAll(async function(){
    await op.restartAll()
    await op.loginPage.logon('test0001','test');
  })

  it('They should see the "admin" tab ', async function() {
    expect(await op.homePage.getAdminTabText()).toEqual('Admin interface')

  });

  it('They should see the "home" tab ', async function() {

    expect(await op.homePage.getHomeTabText()).toEqual('Home')

  });

  


});

describe('When the website is logged in', function() {

  beforeEach(async function(){
    await op.restartAll()
  })

  it('Inactive user should be denied access', async function() {
    await op.loginPage.logon("test0010","test")
    expect(await op.noAccessPage.getAccessText()).toEqual("You do not have access to this page")
  });

  it('User should be able to log out', async function() {
    await op.loginPage.logon("test0001","test")
    await op.homePage.clickLogout("test0001")
    browser.waitForAngularEnabled(false);
    await op.logoutPage.clickLogout();
    await browser.get(browser.params.baseurl);
  });

  it('by an admin they should have access to admin pages', async function() {
    await op.loginPage.logon("test0001","test")
    await op.homePage.clickAdmintab()
    browser.waitForAngularEnabled(false);
    expect(await op.adminPage.getAdminTitleText()).toEqual("EDPC Mentoring Scheme Administration")
  });

  it('by an admin they should have access to matchmaker pages', async function() {
    await op.loginPage.logon("test0001","test")
    await op.homePage.clickMatchMakerTab()
    expect(await op.matchMakerPage.getMatchMakerTitleText()).toEqual("Match mentors and mentees")
  });

  
  it('by an matchmaker they should have access to matchmaker pages', async function() {
    await op.loginPage.logon("test0012","test")
    await op.homePage.clickMatchMakerTab()
    expect(await op.matchMakerPage.getMatchMakerTitleText()).toEqual("Match mentors and mentees")
  });

  it('by an non-admin they should not have access to admin pages', async function() {
    await op.loginPage.logon("test0002","test")
    var items = await op.homePage.getNumAdminTabs()
    expect(items.length).toBe(0)
  });

  it('by an non-matchmaker they should not have access to matchmaker pages', async function() {
    await op.loginPage.logon("test0002","test")
    var items = await op.homePage.getNumMatchMakerTabs()
    expect(items.length).toBe(0)
  });



});
