var objects = new require('./objects.page.js');
var op = new objects();


describe('A matchmaker can', function() {

    beforeEach(async function(){
        await op.restartAll()
      })


      removeAllRelationshipsAndInvites = async function(user,pwd){
        await op.restartAll()
        await op.loginPage.logon(user,pwd)
        var x = await op.homePage.getNumMentorRelationships()
        while (x > 0){
            await op.homePage.removeAMentor()
            await op.restartAll()
            await op.loginPage.logon(user,pwd)
            x = await op.homePage.getNumMentorRelationships()
        }
        var x = await op.homePage.getNumHiddenManageMenteeInvites()
        while (x < 1){
          await op.homePage.clickOnManageMenteeInvites()
          await op.homePage.removeMentorInvite()
          await op.restartAll()
          await op.loginPage.logon(user,pwd)
          x = await op.homePage.getNumHiddenManageMenteeInvites()
      } 
        await op.restartAll()
        await op.loginPage.logon(user,pwd)
        var x = await op.homePage.getNumMenteeRelationships()
        while (x > 0){
            await op.homePage.removeAMentee()
            await op.restartAll()
            await op.loginPage.logon(user,pwd)
            x = await op.homePage.getNumMenteeRelationships()
        }
        var x = await op.homePage.getNumHiddenManageMentorInvites()
        while (x < 1){
          await op.homePage.clickOnManageMentorInvites()
          await op.homePage.removeMenteeInvite()
          await op.restartAll()
          await op.loginPage.logon(user,pwd)
          x = await op.homePage.getNumHiddenManageMentorInvites()
      } 
    }


    it('match a mentor with a mentee', async function() {
        // test0001 is the mentor
        // test0002 is the mentee
        await removeAllRelationshipsAndInvites("test0001","test")
        await removeAllRelationshipsAndInvites("test0002","test")
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.setMenteeSearchingFalse()
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.setMentorSearchingFalse()
        await op.homePage.clickMatchMakerTab()
        await op.matchMakerPage.clickOnName("Alexandra")
        await op.matchMakerPage.clickOnNameInDialog("Verna")
        await op.matchMakerPage.clickOnCreateMentorMentee()
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(1)
        expect(await op.homePage.getNumMenteeRelationships()).toBe(1)
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(1)
        expect(await op.homePage.getNumMentorRelationships()).toBe(1)
    },60000);

    it('match a mentee with a mentor', async function() {
        // test0001 is the mentor
        // test0002 is the mentee
        await removeAllRelationshipsAndInvites("test0001","test")
        await removeAllRelationshipsAndInvites("test0002","test")
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.setMenteeSearchingFalse()
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.setMentorSearchingFalse()
        await op.homePage.clickMatchMakerTab()
        await op.matchMakerPage.clickOnName("Verna")
        await op.matchMakerPage.clickOnNameInDialog("Alexandra")
        await op.matchMakerPage.clickOnCreateMenteeMentor()
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(1)
        expect(await op.homePage.getNumMenteeRelationships()).toBe(1)
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(1)
        expect(await op.homePage.getNumMentorRelationships()).toBe(1)
    },60000);




});