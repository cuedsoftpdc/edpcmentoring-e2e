var objects = new require('./objects.page.js');
var op = new objects();

describe('When a logged in mentee toggles their status ', function() {

    beforeEach(async function(){
        await op.restartAll()
      })

      it('they should be able/not able to write their requirements', async function() {
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingFalse()
        var x = await op.homePage.getNumHiddenMentorReqsCommas()
        expect(x.length).toBe(2)
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.typeInMentorReqs("Written by Daniel")
      });



      it('their status should toggle', async function() {
        await op.loginPage.logon("test0002","test")
        var toggled1 = await op.homePage.getToggleMentorReqsText()
        await op.homePage.clickToggleMentorReqs()
        var toggled2 = await op.homePage.getToggleMentorReqsText()
        expect( (toggled1 == "are" || toggled1 == "are not") && (toggled2 == "are" || toggled2 == "are not") && (toggled1 != toggled2) ).toEqual(true) 
      });

      it('a mentor should see', async function() {
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMentorSearchingTrue()
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.clickInviteMentee()
        var x = await op.homePage.getNumNames("Alexandra")
        expect(x.length).toBe(1)
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMentorSearchingFalse()
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.clickInviteMentee()
        var x = await op.homePage.getNumNames("Alexandra")
        expect(x.length).toBe(0)
      });

});

describe('When a logged in mentor toggles their status ', function() {

    beforeEach(async function(){
      await op.restartAll()
      })

      it('their status should toggle', async function() {
        await op.loginPage.logon("test0002","test")
        var toggled1 = await op.homePage.getToggleMenteeReqsText()
        await op.homePage.clickToggleMenteeReqs()
        var toggled2 = await op.homePage.getToggleMenteeReqsText()
        expect( (toggled1 == "are" || toggled1 == "are not") && (toggled2 == "are" || toggled2 == "are not") && (toggled1 != toggled2) ).toEqual(true) 
      });

      it('a mentee should see', async function() {
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.clickInviteMentor()
        var x = await op.homePage.getNumNames("Alexandra")
        expect(x.length).toBe(1)
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMenteeSearchingFalse()
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.clickInviteMentor()
        var x = await op.homePage.getNumNames("Alexandra")
        expect(x.length).toBe(0)
      });

      it('they should be able/not able to write their requirements', async function() {
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMenteeSearchingFalse()
        var x = await op.homePage.getNumHiddenMenteeReqsCommas()
        expect(x.length).toBe(2)
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.typeInMenteeReqs("Written by Daniel")
      });
});
