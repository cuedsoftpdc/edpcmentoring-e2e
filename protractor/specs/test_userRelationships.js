var objects = new require('./objects.page.js');
var op = new objects();



describe('', function() {

    beforeEach(async function(){
        await op.restartAll()
      })

      removeAllRelationshipsAndInvites = async function(user,pwd){
          await op.restartAll()
          await op.loginPage.logon(user,pwd)
          var x = await op.homePage.getNumMentorRelationships()
          while (x > 0){
              await op.homePage.removeAMentor()
              await op.restartAll()
              await op.loginPage.logon(user,pwd)
              x = await op.homePage.getNumMentorRelationships()
          }
          var x = await op.homePage.getNumHiddenManageMenteeInvites()
          while (x < 1){
            await op.homePage.clickOnManageMenteeInvites()
            await op.homePage.removeMentorInvite()
            await op.restartAll()
            await op.loginPage.logon(user,pwd)
            x = await op.homePage.getNumHiddenManageMenteeInvites()
        } 
          await op.restartAll()
          await op.loginPage.logon(user,pwd)
          var x = await op.homePage.getNumMenteeRelationships()
          while (x > 0){
              await op.homePage.removeAMentee()
              await op.restartAll()
              await op.loginPage.logon(user,pwd)
              x = await op.homePage.getNumMenteeRelationships()
          }
          var x = await op.homePage.getNumHiddenManageMentorInvites()
          while (x < 1){
            await op.homePage.clickOnManageMentorInvites()
            await op.homePage.removeMenteeInvite()
            await op.restartAll()
            await op.loginPage.logon(user,pwd)
            x = await op.homePage.getNumHiddenManageMentorInvites()
        } 
      }

      it('Users Managing relationships 5-8', async function() {
        //test0001 - mentor
        //test0002 - mentee
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.typeInMenteeReqs("sdbhjsdfsdfsdfsdfccjkd") //mentee can see mentor requirements when inviting
        await op.homePage.setMenteeSearchingFalse()
        await op.homePage.setMenteeSearchingTrue()
        await removeAllRelationshipsAndInvites("test0001","test")
        await removeAllRelationshipsAndInvites("test0002","test")
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.clickInviteMentor()
        await op.homePage.clickOnMentorInvite("sdbhjsdfsdfsdfsdfccjkd") //mentee can see mentor requirements when inviting and Mentee can invite mentor
        await op.homePage.clickOnSendInvite() // Mentee can invite mentor
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.clickOnManageMentorInvites()
        await op.homePage.acceptInvite() // mentor can accept invite
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(1) //mentee now on mentor page
        expect(await op.homePage.getNumMenteeRelationships()).toBe(1) //mentee now on mentor page
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(1) // mentor on mentees page
        expect(await op.homePage.getNumMentorRelationships()).toBe(1) // mentor on mentees page
        await op.homePage.removeAMentor() //mentee can remove mentor
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(0)  // mentee no longer on mentor page
        expect(await op.homePage.getNumMentorRelationships()).toBe(0) // mentee no longer on mentor page
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(0) //mentor no longer on mentee page
        expect(await op.homePage.getNumMentorRelationships()).toBe(0) //mentor no longer on mentee page
      },60000);

      it('Users managing relationships 1-4', async function() {
        //test0001 - mentee
        //test0002 - mentor
        await op.loginPage.logon("test0001","test")
        await op.homePage.setMentorSearchingTrue()
        await op.homePage.typeInMentorReqs("sdbhjsdfsdfsdfsdfccjkd") //mentor can see mentee requirements when inviting
        await op.homePage.setMentorSearchingFalse()
        await op.homePage.setMentorSearchingTrue()
        await removeAllRelationshipsAndInvites("test0001","test")
        await removeAllRelationshipsAndInvites("test0002","test")
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        await op.homePage.setMenteeSearchingTrue()
        await op.homePage.clickInviteMentee()
        await op.homePage.clickOnMenteeInvite("sdbhjsdfsdfsdfsdfccjkd") //mentor can see mentee requirements when inviting and Mentor can invite mentee
        await op.homePage.clickOnSendInvite() // Mentor can invite mentee
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        await op.homePage.clickOnManageMenteeInvites()
        await op.homePage.acceptInvite() // mentee can accept invite
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(1) //mentor now on mentee page
        expect(await op.homePage.getNumMentorRelationships()).toBe(1) //mentor now on mentee page
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(1) // mentee on mentors page
        expect(await op.homePage.getNumMenteeRelationships()).toBe(1) // mentee on mentors page
        await op.homePage.removeAMentee() //mentor can remove mentee
        await op.restartAll()
        await op.loginPage.logon("test0001","test")
        expect(await op.homePage.getNumNamePartners("test0002")).toBe(0)  // mentor no longer on mentee page
        expect(await op.homePage.getNumMenteeRelationships()).toBe(0) // mentor no longer on mentee page
        await op.restartAll()
        await op.loginPage.logon("test0002","test")
        expect(await op.homePage.getNumNamePartners("test0001")).toBe(0) //mentee no longer on mentor page
        expect(await op.homePage.getNumMenteeRelationships()).toBe(0) //mentee no longer on mentor page
      },60000);



});